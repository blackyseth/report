import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable{
    private String memberName;
    private List<Client> clients = new ArrayList<Client>();

    public void addClient(Client client){
        clients.add(client);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return null;
    }
}
