import java.util.ArrayList;
import java.util.List;

public class ReportMaker implements Visitor{
    private int numberOfClients;
    private int numberOfOrders;
    private int numberOfProducts;
    private String report;

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public int getNumberOfOrders() {
        return numberOfOrders;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfClients(int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public void setNumberOfOrders(int numberOfOrders) {
        this.numberOfOrders = numberOfOrders;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public String showReport(){
        return report;
    }

    @Override
    public void visit(Visitable v) {
        List<Object> list = new ArrayList<Object>();
        System.out.println(list.size());
    }
}
