import java.util.ArrayList;
import java.util.List;

public class Client implements Visitable{
    private String number;
    private List<Order> orders = new ArrayList<Order>();

    public String getClientNumber() {
        return number;
    }

    public void setClientNumber(String number) {
        this.number = number;
    }

    public void addOrder(Order order){
        orders.add(order);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return null;
    }
}
