import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order implements Visitable{

    private String number;
    private double orderTotalPrice;
    private Date date;
    private List<Product> products = new ArrayList<Product>();

    public String getOrderNumber() {
        return number;
    }

    public double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public Date getOrderDate() {
        return date;
    }

    public void addProduct(Product product){
        products.add(product);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return null;
    }
}
